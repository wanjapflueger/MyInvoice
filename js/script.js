jQuery(document).ready(function() {

  $('#addmore').click(function (e) {
    e.preventDefault();
    var invoiceitems = $('#invoiceitems');
    var num = +invoiceitems.attr("data-i");
    console.log(num);
    num = num + 1;
    console.log(num);
    invoiceitems.attr('data-i', num);

    var newNum = num - 1;

    var newItem = ''
    + '<div class="field-group inline">'
    + '<div class="field-item">'
    + '<label for="invoiceItems[' + newNum + '][orderContent]">Artikel</label>'
    + '<input type="text" value="" id="invoiceItems[' + newNum + '][orderContent]" name="invoiceItems[' + newNum + '][orderContent]" />'
    + '</div>'
    + '<div class="field-item">'
    + '<label for="invoiceItems[' + newNum + '][orderAmount]">Menge</label>'
    + '<input type="text" value="" id="invoiceItems[' + newNum + '][orderAmount]" name="invoiceItems[' + newNum + '][orderAmount]" />'
    + '</div>'
    + '<div class="field-item">'
    + '<label for="invoiceItems[' + newNum + '][orderValue]">Betrag in EUR</label>'
    + '<input type="text" value="" id="invoiceItems[' + newNum + '][orderValue]" name="invoiceItems[' + newNum + '][orderValue]" />'
    + '</div>'
    + '<div class="field-item">'
    + '<label for="invoiceItems[' + newNum + '][orderDiscount]">Rabatt in %</label>'
    + '<input type="text" value="" id="invoiceItems[' + newNum + '][orderDiscount]" name="invoiceItems[' + newNum + '][orderDiscount]" />'
    + '</div>'
    + ''
    + '</div>';
    invoiceitems.append(newItem);
  });

  $('.removeitem').on('click',function() {
    $(this).parent().remove();
  });

  // Replace comma with dot in .orderValueComma (item.php)
  $('.orderValueComma').on('keyup',function() {
    $(this).val($(this).val().replace(/,/g, '.'));
  });

});
