<? header('Content-type: text/html; charset=UTF-8') ;
require_once('config.php');
require_once("dompdf/autoload.inc.php");
// reference the Dompdf namespace
use Dompdf\Dompdf;

/**
* Invoice generator
*/

// JSON File
$jsonFile = $conf['json']['data'];
$jsonFileContact = $conf['json']['contact'];

if(!file_exists($jsonFile) OR !file_exists($jsonFileContact)) {
  die;
}

// GET Data Invoice
$jsonString = file_get_contents($jsonFile);
$data = json_decode($jsonString, true);

// GET Data Contact
$jsonStringContact = file_get_contents($jsonFileContact);
$data_contact = json_decode($jsonStringContact, true);

foreach ($data_contact as $key => $user) {
  $userName = $user["name"];
  $userAddress = $user["address"];
  $userZip = $user["zip"];
  $userCity = $user["city"];
  $userPhone = $user["phone"];
  $userMail = $user["mail"];
  $userDomain = $user["domain"];
  $userStnr = $user["stnr"];
  $userBank = $user["bank"];
  $userIban = $user["iban"];
  $userBic = $user["bic"];
  $userNoMwst = $user["nomwst"];
}

if($_GET["id"]) {
  // Create new file
  $dir = $conf['media']['pdf'].$_GET["id"].'/';
  $ext = '.pdf';
  $filename = $_GET["id"];
} else {
  die;
}

foreach ($data as $key => $item) {
  if ($item["id"] == $_GET["id"]) {

    // HTML
    $html = '';
    $html .= '<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">';
    $html .= '<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="en" lang="en">';
    $html .= '<head>';
    $html .= '<title>'.$item["id"].'.pdf</title>';
    $html .= '<meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />';

    // CSS (DIN A4 Hochformat: 210x297mmm)
    $html .= '<style>';
    $html .= file_get_contents($conf['css']['print']);
    $html .= '</style>';
    $html .= '</head>';
    // BODY
    $html .= '<body>';

    // Layout elements
    $html .= '<div class="falzmarke fmpos-1"></div>';
    $html .= '<div class="falzmarke fmpos-2"></div>';
    $html .= '<div class="lochmarke lm-top"></div>';
    $html .= '<div class="lochmarke lm-center"></div>';
    $html .= '<div class="lochmarke lm-bottom"></div>';
    $html .= '<div class="design-layer"></div>';

    /**
    * // =========== // *
    * // PDF Content // *
    * // =========== // *
    */

    // Main
    $html .= '<div class="main">';

    // vCard -> {domain}/vCard.vcf
    $html .= '<div class="qr-code">';
    $html .= '<img alt="QR" src="'.$conf['qr'].'" />';
    $html .= '<span>'.$conf['qr-caption'].'</span>';
    $html .= '</div>';

    // Logo
    $html .= '<img class="logo" alt="Logo" src="'.$conf['logo'].'" />';

    // Empfänger
    $html .= '<div class="empfaenger">';
    if($item["company"]) {
      $html .= '<span class="company">'.$item["company"].'</span>';
    }
    $html .= '<span class="name">'.$item["firstName"] . ' ' . $item["lastName"] . '</span>';
    $html .= '<span class="street">'.$item["street"].'</span>';
    $html .= '<span class="zip-city">' . $item["zip"] . ' ' . $item["city"] . '</span>';
    $html .= '</div>'; // .empfaenger - End

    // Rechnungsnummer
    $html .= '<span class="label-rechnungsnummer">Rechnungsnummer</span>';
    $html .= '<span class="rechnungsnummer">'.$item["id"].'</span>';

    // Projekt
    $html .= '<div class="project">';
    $html .= '<span class="projectname"><strong>Projektname</strong> ' . $item["projectName"] . '</span>';
    $html .= '<span class="projectdesc"><strong>Beschreibung</strong> ' . $item["projectDesc"] . '</span>';
    $html .= '<span class="projectdesc"><strong>Zahlungsziel</strong> 7 Tage</span>';
    $html .= '</div>'; // .project - End

    $html .= '</div>'; // .main - End

    // Aside
    $html .= '<div class="aside">'; // .aside

    $html .= '<span class="rechnung">Rechnung</span>';

    $html .= '<span class="date">'.date('d.m.Y', strtotime($item["dateCreated"])).'</span>';

    $html .= '<span class="address">';
    $html .= '<strong>'.$userName.'</strong>' . '<br />';
    $html .= $userAddress . '<br />';
    $html .= $userZip . ' ' . $userCity;
    $html .= '</span>';

    $html .= '<span class="contact">';
    $html .= $userPhone . '<br />';
    $html .= $userMail . '<br />';
    $html .= $userDomain;
    $html .= '</span>';

    $html .= '<span class="stnr">StNr.: '.$userStnr.'</span>';

    $html .= '</div>'; // .aside - End

    // Clear float
    $html .= '<div style="clear:both"></div>';

    // Kostenübersicht
    $html .= '<div class=table-kostenuebersicht>';
    $html .= '<table width="100%">';
      $html .= '<thead>';

        $html .= '<tr>';
          $html .= '<td>Pos</td>';
          $html .= '<td class="article">Artikel</td>';
          $html .= '<td>Menge</td>';
          $html .= '<td>Rab.%</td>';
          $html .= '<td>Preis/St.</td>';
          $html .= '<td>Kosten</td>';
        $html .= '</tr>';

      $html .= '</thead>';
      $html .= '<tbody>';

        $i = 1;

        foreach ($item["invoiceItems"] as $key => $invoiceItem) {

        $html .= '<tr>';
          $html .= '<td>' . $i . '</td>'; // Pos
          $html .= '<td>' . $invoiceItem["orderContent"] . '</td>'; // Beschreibung
          $html .= '<td>' . $invoiceItem["orderAmount"] . '</td>'; // Menge
          $html .= '<td>';
          if($invoiceItem["orderDiscount"] > 0){
            $html .= number_format($invoiceItem["orderDiscount"],2); // Rab.%
          }
          $html .= '</td>';
          $html .= '<td>€ ' . number_format($invoiceItem["orderValue"] / (1 + $item["vat"]/100), 2) . '</td>'; // Preis/St.
          $html .= '<td>€ ' . number_format((($invoiceItem["orderValue"] / (1 + $item["vat"]/100)) * $invoiceItem["orderAmount"]) * ((-1 + $invoiceItem["orderDiscount"]/100) * -1),2) . '</td>'; // Kosten inkl. Rabatt
        $html .= '</tr>';

        // Netto Gesamtbetrag
        $netto_gesamt += ($invoiceItem["orderValue"] / (1 + $item["vat"]/100)) * $invoiceItem["orderAmount"] * ((-1 + $invoiceItem["orderDiscount"]/100) * -1);

        // Brutto Gesamtbetrag
        $brutto_gesamt += $invoiceItem["orderValue"] * $invoiceItem["orderAmount"] * ((-1 + $invoiceItem["orderDiscount"]/100) * -1);

        $i++;
        }

        $html .= '<tr>';
          $html .= '<td></td>';
          $html .= '<td></td>';
          $html .= '<td></td>';
          $html .= '<td></td>';
          $html .= '<td></td>';
          $html .= '<td></td>';
        $html .= '</tr>';
        $html .= '<tr>';
          $html .= '<td></td>';
          $html .= '<td></td>';
          $html .= '<td></td>';
          $html .= '<td></td>';
          $html .= '<td></td>';
          $html .= '<td></td>';
        $html .= '</tr>';

        $html .= '<tr>';
          $html .= '<td></td>';
          $html .= '<td></td>';
          $html .= '<td></td>';
          $html .= '<td></td>';
          $html .= '<td><strong>Netto</strong></td>';
          $html .= '<td><strong>€ ';
          $html .= number_format($netto_gesamt,2);
          $html .= '</strong></td>';
        $html .= '</tr>';

        $html .= '<tr>';
          $html .= '<td></td>';
          $html .= '<td></td>';
          $html .= '<td></td>';
          $html .= '<td>MwSt.</td>';
          if($item["vat"]) {
            $html .= '<td>' . $item["vat"] . '%</td>';
          } else {
            $html .= '<td>0%</td>';
          }
          $html .= '<td>€ ';
          // Brutto - Netto Gesamtbetrag
          $nettoWert = 0;
          $nettoWert += $brutto_gesamt - $netto_gesamt;
          $html .= number_format($nettoWert,2);
          $html .= '</td>';
        $html .= '</tr>';

        $html .= '<tr class="last">';
          $html .= '<td>Gesamtbetrag (Brutto)</td>';
          $html .= '<td></td>';
          $html .= '<td></td>';
          $html .= '<td></td>';
          $html .= '<td></td>';
          $html .= '<td>€ ';
          $html .= number_format($brutto_gesamt,2);
          $html .= '</td>';
        $html .= '</tr>';

      $html .= '</tbody>';
    $html .= '</table>';

    $html .= '</div>'; // end .table-kostenuebersicht

    $html .= '<div style="page-break-after: auto;"></div>';

    $html .= '<div class="main">';

    // Comment
    if($item['comment']) {
      $html .= '<div class="comment">';
      $html .= $item['comment'];
      $html .= '</div>';
    }

    // Anweisung
    $html .= '<div class="anweisung">';
    $html .= '<p>';
    $html .= 'Bitte überweisen Sie den genannten Betrag innerhalb der nächsten 7- spätestens aber bis zum 14. Folgetag auf nachfolgendes Konto.';
    $html .= '</p>';
    if($item["vat"] == 0) {
      $html .= '<p>';
      $html .= $userNoMwst;
      $html .= '</p>';
    }
    $html .= '<p>';
    $html .= 'Kreditinstitut ';
    $html .= '<strong>'.$userBank.'</strong>';
    $html .= '</p>';
    $html .= '<p>';
    $html .= 'IBAN ';
    $html .= '<strong>'.$userIban.'</strong>';
    $html .= '<br />';
    $html .= 'BIC ';
    $html .= '<strong>'.$userBic.'</strong>';
    $html .= '</p>';

    $html .= '</div>'; // .anweisung - End

    // Anweisung
    $html .= '<div class="anweisung">';
    $html .= '
    <p>
    Mit freundlichen Grüßen,<br />
    '.$userName.'
    </p>
    ';
    $html .= '</div>'; // .anweisung - End

    $html .= '</div>'; // .main - End

    // Clear float
    $html .= '<div style="clear:both"></div>';

    // Footer
    $html .= '<div class="footer">'; // .footer
    $html .= $userDomain;
    $html .= ' • ';
    $html .= date('m/Y', strtotime($item["dateCreated"]));
    $html .= ' • ';
    $html .= $item["id"];
    $html .= '</div>'; // .footer - End

    /**
    * // =========== //
    */
    $html .= '</body>';
    $html .= '</html>';

    /**
    * Stream PDF
    *
    * Generate and download PDF
    */
    // instantiate and use the dompdf class
    $dompdf = new Dompdf();
    $dompdf->loadHtml($html, 'HTML-ENTITIES', 'UTF-8');

    // (Optional) Setup the paper size and orientation
    $dompdf->setPaper('A4', 'portrait');

    // Render the HTML as PDF
    $dompdf->render();

    // Output the generated PDF to Browser
    // 0 -> Output in Browser (for dev)
    // 1 -> Download
    $dompdf->stream($filename, array("Attachment" => 1));

    /**
    * Save PDF
    *
    * Save PDF to your server
    */
    // instantiate and use the dompdf class
    $dompdfOutput = new Dompdf();
    $dompdfOutput->loadHtml($html, 'HTML-ENTITIES', 'UTF-8');

    // (Optional) Setup the paper size and orientation
    $dompdfOutput->setPaper('A4', 'portrait');

    // Render the HTML as PDF
    $dompdfOutput->render();
    // Save file to server
    $output = $dompdfOutput->output();
    file_put_contents($dir.$filename.$ext, $output);
  }
}
