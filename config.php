<?php

define('_WEXEC', 1);

/**
* Configuration
*/

// Charset
$conf['charset'] = 'utf-8';

// App name
$conf['app-name'] = 'My Invoice Tool';

// Start date (Y) for filter function in items.php, e.g.: 2013
$conf['startdate'] = 2015;

// Default Invoice-ID
$conf['default-invoice-id'] = "WP_XXX_" . date("m_Y_") . substr(str_shuffle(str_repeat("123456789WFP", 5)), 0, 5);

/**
* Files and directories
*/
# css
$conf['css']['dir'] = 'css/'; // Directory
$conf['css']['template'] = $conf['css']['dir'] . 'template.css.php';
$conf['css']['print'] = $conf['css']['dir'] . 'print.css'; // included in invoice.php

# javascript
$conf['js']['dir'] = 'js/'; // Directory
$conf['js']['logic'] = $conf['js']['dir'] . 'script.js'; // Custom JS

# json
$conf['json']['dir'] = 'data/'; // Directory
$conf['json']['data'] = $conf['json']['dir'] . 'data.json'; // Invoice Data JSON
$conf['json']['backup'] = $conf['json']['dir'] . 'backup.json'; // Backup deleted items from $conf['json']['data'] to this file
$conf['json']['contact'] = $conf['json']['dir'] . 'contact.json'; // This is your vCard information

# media
$conf['media']['dir'] = 'media/'; // Directory
$conf['media']['pdf'] = $conf['media']['dir'] . 'pdf/'; // Dir for pdf files
$conf['media']['images'] = $conf['media']['dir'] . 'images/'; // Dir for images
$conf['logo'] = $conf['media']['images'] . 'logo.png'; // My Logo
$conf['qr'] = $conf['media']['images'] . 'qr-code.png'; // QR Code
$conf['qr-caption'] = 'vCard'; // QR Code caption
