<?php defined('_WEXEC') or die;

/**
* Current item
*
* @method GET
* @name id
* @class updateform
*/

// GET JSON
foreach ($data as $key => $item):

// GET JSON WHERE item id
if ($item["id"] == $_GET["id"]):

?>
<div class="updateform">
  <h2><?php echo $item["id"]; ?></h2>
  <form action="" method="post">

    <?php
    /**
    * Status
    *
    * Message if item is trashed
    */
    if($item["status"] == -1):
    ?>
    <ul class="info">
      <li>
        Deleted on: <?php echo date('d.m.Y H:i\h', strtotime($item["dateDeleted"])); ?>
      </li>
    </ul>
    <?php endif; ?>

    <?php
    /**
    * Toolbar
    *
    * Save, Create PDF, Trash, Restore, Delete
    */
    ?>
    <div class="submit">

      <input type="submit" name="update" class="save" value="Save" />

      <?php if(!file_exists("media/pdf/" . $item["id"] . "/" .$item["id"].'.pdf')): // only show this if pdf does not exist ?>
        <input type="submit" name="pdf" class="pdf" value="Create PDF" />
      <?php endif; ?>

      <?php if(file_exists("media/pdf/" . $item["id"] . "/" .$item["id"].'.pdf')): // only show this if pdf exists ?>
      <input type="submit" name="download" class="download" value="Download" />
      <input type="submit" name="updatepdf" class="updatepdf" value="Update PDF" />
      <?php endif; ?>

      <?php if($item["status"] == -1): // if trashed ?>
        <input type="submit" name="restore" class="restore" value="Restore" id="restoreItem"/>
        <input type="submit" name="delete" class="delete" value="Delete" id="delete" onClick="javascript: return confirm('Attention! This action can not be undone. PDF files are not being removed from the server.');"/>
      <?php else: ?>
        <input type="submit" name="trash" class="trash" value="Trash" id="trash"/>
      <?php endif; ?>

      <a class="close" href="index.php#<?php echo $item["id"]; ?>">Close</a>
    </div>

    <?php
    /**
    * Form input fields
    */
    ?>
    <div class="field-list">

      <div class="field-group inline">
        <div class="field-item">
          <label for="id">Rechnungsnummer</label>
          <input type="text" value="<?php echo $item["id"]; ?>" id="id" name="id" />
        </div>
        <div class="field-item">
          <label for="dateCreated">Datum</label>
          <input type="text" value="<?php echo $item["dateCreated"]; ?>" id="dateCreated" name="dateCreated" />
        </div>
      </div>

      <div class="field-group">
        <div class="field-item">
          <label for="company">Firma</label>
          <input type="text" value="<?php echo $item["company"]; ?>" id="company" name="company" />
        </div>
      </div>

      <div class="field-group inline">
        <div class="field-item">
          <label for="firstName">Vorname</label>
          <input type="text" value="<?php echo $item["firstName"]; ?>" id="firstName" name="firstName" />
        </div>
        <div class="field-item">
          <label for="lastName">Nachname</label>
          <input type="text" value="<?php echo $item["lastName"]; ?>" id="lastName" name="lastName" />
        </div>
      </div>

      <div class="field-group inline">
        <div class="field-item">
          <label for="street">Straße</label>
          <input type="text" value="<?php echo $item["street"]; ?>" id="street" name="street" />
        </div>
        <div class="field-item">
          <label for="zip">PLZ</label>
          <input type="text" value="<?php echo $item["zip"]; ?>" id="zip" name="zip" />
        </div>
        <div class="field-item">
          <label for="city">City</label>
          <input type="text" value="<?php echo $item["city"]; ?>" id="city" name="city" />
        </div>
      </div>

      <div class="field-group">
        <div class="field-item">
          <label for="projectName">Projektname</label>
          <input type="text" value="<?php echo $item["projectName"]; ?>" id="projectName" name="projectName" />
        </div>
        <div class="field-item">
          <label for="projectDesc">Projektbeschreibung</label>
          <input type="text" value="<?php echo $item["projectDesc"]; ?>" id="projectDesc" name="projectDesc" />
        </div>
      </div>

      <div id=invoiceitems data-i=<?php echo count($item["invoiceItems"]); ?>>
        <?php $i = 0; ?>
        <?php if($item["invoiceItems"]): ?>
          <?php foreach ($item["invoiceItems"] as $invoiceItem): ?>
            <div class="field-group inline">
              <div class="field-item">
                <label for="invoiceItems[<?php echo $i; ?>][orderContent]">Artikel</label>
                <input type="text" value="<?php echo $invoiceItem["orderContent"]; ?>" id="invoiceItems[<?php echo $i; ?>][orderContent]" name="invoiceItems[<?php echo $i; ?>][orderContent]" />
              </div>
              <div class="field-item">
                <label for="invoiceItems[<?php echo $i; ?>][orderAmount]">Menge</label>
                <input type="text" value="<?php echo $invoiceItem["orderAmount"]; ?>" id="invoiceItems[<?php echo $i; ?>][orderAmount]" name="invoiceItems[<?php echo $i; ?>][orderAmount]" />
              </div>
              <div class="field-item">
                <label for="invoiceItems[<?php echo $i; ?>][orderValue]">Betrag in EUR</label>
                <input class="orderValueComma" type="text" value="<?php echo str_replace(',','.',$invoiceItem["orderValue"]); ?>" id="invoiceItems[<?php echo $i; ?>][orderValue]" name="invoiceItems[<?php echo $i; ?>][orderValue]" />
              </div>
              <div class="field-item">
                <label for="invoiceItems[<?php echo $i; ?>][orderDiscount]">Rabatt in %</label>
                <input type="text" value="<?php echo $invoiceItem["orderDiscount"]; ?>" id="invoiceItems[<?php echo $i; ?>][orderDiscount]" name="invoiceItems[<?php echo $i; ?>][orderDiscount]" />
              </div>
              <div class=removeitem>X</div>
            </div>
            <?php $i++; ?>
          <?php endforeach; ?>
        <?php endif; ?>
      </div>
      <div id=addmore>Add Pos</div>


      <div class="field-group inline">
        <div class="field-item">
          <label for="vat">MwSt.</label>
          <input type="text" value="<?php echo $item["vat"]; ?>" id="vat" name="vat" />
        </div>
      </div>

      <!-- Kommentar -->
      <div class="field-group">
        <div class="field-item">
          <label for="comment">Kommentar</label>
          <input type="text" value="<?php echo $item["comment"]; ?>" id="comment" name="comment" />
        </div>
      </div>


    </div><!-- // .field-list -->


    <input type="text" hidden value="<?php echo $key; ?>" name="key" id="key" />

    <ul class="info">
      <li>
        <strong>Last modified</strong>
        <?php echo date('d.m.Y H:i\h', strtotime($item["dateModified"]));?>
      </li>
      <li>
        <strong>Created</strong>
        <?php echo date('d.m.Y H:i\h', strtotime($item["dateCreated"])); ?>
      </li>
    </ul>

  </form>
</div>

<?php
endif;
endforeach;
?>
