# MyInvoice-Tool
With this tool you can manage your invoice data, create PDFs from a template and nothing else. There are no unnecessary additional functions and the tool is based on PHP.

**This tool actually works**, but it's still in production, so there will be updates from time to time.

## Requirements
To use this tool you will need **PHP** on your server or local machine.

___

[TOC]

___

## Configuration
In *config.php* you can change anything, such as the **app name** or the **default invoice number**.
Be careful when changing file paths!

### Color scheme
If you want to change the base color for the color scheme, go to /css/template.less and change `@color_base` in line 16 to whatever you want.

## vCard
Keep your vCard up to date. The data in PDF files is only overwritten if you overwrite the PDF.

### Logo
Path: `$conf['logo']`

### QR Code
**Example usage**
Include a Link to your personal vCard file (example.com/vCard.vcf).

Path: `$conf['qr']`

You may generate your own QR Code with [Googles API](https://developers.google.com/chart/infographics/docs/qr_codes) and save it to your server. *DomPDF* can't load files from external servers, so the file must first be saved on your server.

## New Invoice
Hit **+ Create new invoice** to automatically generate a new entry in `$conf['json']['data']`.

### Create PDF
Before you can download your PDF as a file, you have to generate it by hitting **Create PDF**.

**New File path**
```
$conf['media']['pdf']/{Rechnungsnummer}/{Rechnungsnummer}.pdf
```


## Update Invoice
Updating your invoice data don't mean you updated your invoice PDF. To do so you have to hit **Update PDF**.

### Overwrite PDF
If you update a PDF, the old one is still available in the same directory. The
default PDF wich can be downloaded by hitting the button **Download** is located at

**Backup File path**
```
$conf['media']['pdf']/{Rechnungsnummer}/{Rechnungsnummer}_{timestamp_on_deletion}.pdf
```

### Upload existing PDF
Just rename your PDF to `{Rechnungsnummer}.pdf` and upload it to `$conf['media']['pdf']/{Rechnungsnummer}/`.

## Trash and Delete invoice
1. Your PDF file will never be removed. You have to take care of that yourself.
2. Trashed items can be restored easily
3. Deleted items are removed from `$conf['json']['data']` and put into `$conf['json']['backup']`

## Security
**Use password authentication for this tool.** There is no built-in feature. [Visit this link](http://www.lmgtfy.com/?q=how+to+setup+password+authentication) to get more information about password protected directories.

Use `defined('_WEXEC') or die;` on the start of every PHP file you create to ensure they are only executable with `include()` and `require()` in your **index.php**. [Visit this page](https://docs.joomla.org/JEXEC) for more information about basically the same principle.

```
<?php
defined('_WEXEC') or die('Restricted access');
?>
<html>
<body>
...
```

### Personal Data
Your personal data is yours and nobody wants to know what you do with it anyway. Means that there is no connection to the outside world in this tool.

In case you want to backup your personal data yourself, all your need to backup is this:

```/$conf['media']['pdf']/```

and
```config.php```

and
```/$conf['json']['dir']/```

___


![](https://i.creativecommons.org/l/by/4.0/88x31.png)
Invoice Tool von Wanja Friedemann Pflüger ist lizenziert unter einer [Creative Commons Namensnennung 4.0 International Lizenz](http://creativecommons.org/licenses/by/4.0/).

Innsbruck (AT), 11.02.2018
wanjapflueger.de
