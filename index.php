<?php

require_once('config.php');

/**
* Invoice Tool
*
* @author Wanja Friedemann Pflüger
* @version 1.0
* @created 02.02.2018
* @lastModified 08.02.2018
*
* This tool lets you create invoices,
* stores them as JSON Objects in $jsonFile
* and creates PDF-Files using dompdf.
*
* Adding items
*
* If you want to add fields to your invoice
* you will need to adjust that in
* /item.php
* /methods/update.php
* /methods/delete.php
*/

// Timezone
date_default_timezone_set('Europe/Berlin');

/**
* GET JSON
*
* @content invoice data
*/
$jsonFile = "data/data.json"; // Filename
$jsonString = file_get_contents($jsonFile); // JSON String
$data = json_decode($jsonString, true); // data array


/**
* Function: sortFunction
*
* Sorts array by value
*/
function sortFunction( $a, $b ) {
  return strtotime($b["dateCreated"]) - strtotime($a["dateCreated"]);
}
usort($data, "sortFunction"); // Sort $data by "dateCreated"


/**
* Function: Alert
*/
function alert($msg) {
  echo '<div class="alert">' . $msg . '</div>';
}

/**
* Methods
*
* @dir /methods/
* On Form Submit
*/

// new
if (isset($_POST["new"])) {
  require('methods/new.php');
}

// update
elseif(isset($_POST["update"])) {
  require('methods/update.php');
}

// get vcard
elseif(isset($_POST["vcard"])) {
  require('methods/vcard.php');
}

// update vcard
elseif(isset($_POST["updatevcard"])) {
  require('methods/updatevcard.php');
}

// create pdf
elseif (isset($_POST["pdf"])) {
  require('methods/create_pdf.php');
}

// update pdf
elseif (isset($_POST["updatepdf"])) {
  require('methods/update_pdf.php');
}

// download
elseif (isset($_POST["download"])) {
  require('methods/download_pdf.php');
}

// trash
elseif (isset($_POST["trash"])) {
  require('methods/trash.php');
}

// delete
elseif (isset($_POST["delete"])) {
  require('methods/delete.php');
}

// restore
elseif (isset($_POST["restore"])) {
  require('methods/restore.php');
}
?>
<?php // Begin HTML ?>
<!doctype html>
<html>
<head>
  <meta charset="<?php echo $conf['charset']; ?>" />
  <title><?php echo $conf['app-name']; ?></title>
  <link href="<?php echo $conf['css']['template']; ?>" rel="stylesheet" type="text/css" />
  <meta name="viewport" content="width=device-width, initial-scale=1.0">
  <link href="https://use.fontawesome.com/releases/v5.0.6/css/all.css" rel="stylesheet">
  <script src="<?php echo $conf['js']['dir']; ?>jquery.min.js"></script>
</head>
<body>

  <!-- Header -->
  <header>
    <span class="curdate"><?php echo date("l jS \of F Y h:i:s A"); ?></span>
    <h1><a href="index.php">#</a> <?php echo $conf['app-name']; ?></h1>
    <nav>
      <ul>
        <li>
          <form action="" method="post">
            <input type="submit" name="new" value="+ Create new invoice" />
            <input type="submit" name="vcard" value="My vCard" />
          </form>
        </li>
      </ul>
    </nav>
  </header>
  <!-- // Header -->

  <!-- Main -->
  <main>
    <?php
    // Update Conctact
    if($_GET["page"] == 'contact') {
      include 'contact.php';
    }
    if($_GET["id"]) {
      include 'item.php'; // Current item by Invoice id
    } else {
      include 'items.php'; // List of all items
    }
    ?>
  </main>

  <script src="<?php echo $conf['js']['logic']; ?>"></script>
</body>
</html>
