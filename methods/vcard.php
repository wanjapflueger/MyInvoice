<?php defined('_WEXEC') or die;

/**
* vCard
*
* @on Form submit
* @method _POST
* @name vcard
*/

$contactFile = $conf['json']['contact'];

// If file exists
if(file_exists($contactFile)) {

  // Redirect after Submit
  header('Location: index.php?page=contact');
} else {
  echo $contactFile . ' not found';
  die;
}
