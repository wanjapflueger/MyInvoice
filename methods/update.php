<?php defined('_WEXEC') or die;

/**
* Update
*
* @on Form submit
* @method _POST
* @name update
*/

// If file exists
if(file_exists($jsonFile)) {

  // Values
  $data[$_POST["key"]]['id'] = $_POST["id"];
  $data[$_POST["key"]]['firstName'] = $_POST["firstName"];
  $data[$_POST["key"]]['lastName'] = $_POST["lastName"];
  $data[$_POST["key"]]['company'] = $_POST["company"];

  $data[$_POST["key"]]['street'] = $_POST["street"];
  $data[$_POST["key"]]['zip'] = $_POST["zip"];
  $data[$_POST["key"]]['city'] = $_POST["city"];

  $data[$_POST["key"]]['projectName'] = $_POST["projectName"];
  $data[$_POST["key"]]['projectDesc'] = $_POST["projectDesc"];

  $data[$_POST["key"]]['comment'] = $_POST["comment"];

  if($_POST["vat"]) {
    $data[$_POST["key"]]['vat'] = $_POST["vat"];
  } else {
    $data[$_POST["key"]]['vat'] = 0;
  }

  $data[$_POST["key"]]['invoiceItems'] = $_POST["invoiceItems"]; // Invoice items

  $data[$_POST["key"]]['dateModified'] = date("c");
  $data[$_POST["key"]]['dateCreated'] = $_POST["dateCreated"];

  // Put contents into JSON
  $newJsonString = json_encode($data);
  file_put_contents($jsonFile, $newJsonString);

  // Redirect after Submit
  header('Location: index.php?id='.$_POST["id"]);
}
