<?php defined('_WEXEC') or die;

/**
* Delete
*
* @on Form submit
* @method _POST
* @name delete
*/

$jsonFileBackup = $conf['json']['backup']; // Filename Backup

// Backup first
if(file_exists($jsonFileBackup)) {

  $backupCreated = date("c"); // Current date ISO 8601

  // Values for Backup
  $addDataArray[] = array(
    'id' => $_POST["id"],
    'firstName' => $_POST["firstName"],
    'lastName' => $_POST["lastName"],
    'company' => $_POST["company"],
    'street' => $_POST["street"],
    'zip' => $_POST["zip"],
    'city' => $_POST["city"],
    'projectName' => $_POST["projectName"],
    'projectDesc' => $_POST["projectDesc"],
    'vat' => $_POST["vat"],
    'comment' => $_POST["comment"],
    'invoiceItems' => $_POST["invoiceItems"],
    'dateModified' => $_POST["dateModified"],
    'dateCreated' => $_POST["dateCreated"],
    'dateDeletedForGood' => $backupCreated
  );
  $jsonStringBackup = file_get_contents($jsonFileBackup); // Open Backupfile
  $dataBackup = json_decode($jsonStringBackup, true); // data Backup array

  $dataBackupJSON = json_encode(array_merge($dataBackup,$addDataArray)); // Merge existing backup file with deleted item

  // Add deleted item to $jsonFileBackup
  file_put_contents($jsonFileBackup, $dataBackupJSON);

}

// If file exists remove it from $jsonFile
if(file_exists($jsonFile)) {

  // Values
  unset($data[$_POST["key"]]);

  // Put contents into JSON
  $newJsonString = json_encode($data);
  file_put_contents($jsonFile, $newJsonString);

  // Redirect after Submit
  header('Location: index.php');
}
