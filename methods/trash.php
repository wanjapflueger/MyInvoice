<?php defined('_WEXEC') or die;

/**
* Trash
*
* @on Form submit
* @method _POST
* @name trash
*/

// If file exists
if(file_exists($jsonFile)) {

  // Values
  $data[$_POST["key"]]['status'] = "-1"; // Change status of invoice to 'trashed'
  $data[$_POST["key"]]['dateDeleted'] = date(c); // Add Timstamp for deletion

  // Put contents into JSON
  $newJsonString = json_encode($data);
  file_put_contents($jsonFile, $newJsonString);

  // Redirect after Submit
  header('Location: index.php?id='.$_POST["id"]);

}
