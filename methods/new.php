<?php defined('_WEXEC') or die;

/**
* New invoice
*
* @on Form submit
* @method _POST
* @name new
*/

$newID = $conf['default-invoice-id']; // Default Invoice-ID
$newCreated = date("c"); // Current date ISO 8601

$addDataArray[] = array(
  'id' => $newID,
  'dateCreated' => $newCreated,
  'invoiceItems' => array(array("orderContent"=>"","orderAmount"=>"","orderDiscount"=>"","orderValue"=>""))
);

// If file exists
if(file_exists($jsonFile)) {
  // Put contents into JSON and merge them to existing data
  $addData = json_encode(array_merge($data,$addDataArray));
  file_put_contents($jsonFile, $addData);

  // Redirect after Submit
  header('Location: index.php?id='.$newID);
}
