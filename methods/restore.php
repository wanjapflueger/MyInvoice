<?php defined('_WEXEC') or die;

/**
* Restore trashed item
*
* @on Form submit
* @method _POST
* @name restore 
*/

// If file exists
if(file_exists($jsonFile)) {

  // Values
  $data[$_POST["key"]]['status'] = "1";
  $data[$_POST["key"]]['daterestoreed'] = date(c);

  // Put contents into JSON
  $newJsonString = json_encode($data);
  file_put_contents($jsonFile, $newJsonString);

  // Redirect after Submit
  header('Location: index.php?id='.$_POST["id"]);
}
