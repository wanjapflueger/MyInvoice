<?php defined('_WEXEC') or die;

/**
* Create PDF
*
* @on Form submit
* @method _POST
* @name pdf
*/

$newDir = $conf['media']['pdf'].$_POST["id"]; // Directory including invoice-ID

if (!file_exists($newDir)) {
  mkdir($newDir, 0777, true); // Create folder if not exists
}

// Redirect after Submit
header('Location: invoice.php?id='.$_POST["id"]);
