<?php defined('_WEXEC') or die;

/**
* Update PDF and Backup old version
*
* @on Form submit
* @method _POST
* @name updatepdf
*/

// find old file
$oldpdf = $conf['media']['pdf'] . $_POST["id"] . "/" .$_POST["id"] . '.pdf';

// new name for old file with timestamp
$archiveFilename = $conf['media']['pdf'] . $_POST["id"] . "/" .$_POST["id"] . "_backup_" . date(c) . '.pdf';

// rename and backup old file (same directory)
if(file_exists($oldpdf)) {
  rename($oldpdf, $archiveFilename);
}

// Redirect after Submit
header('Location: invoice.php?id='.$_POST["id"]);
