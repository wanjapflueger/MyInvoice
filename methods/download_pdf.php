<?php defined('_WEXEC') or die;

/**
* Download PDF
*
* @on Form submit
* @method _POST
* @name download
*/

// Header Content-type
header("Content-type:application/pdf");
// Set filename
header("Content-Disposition:attachment;filename='".$_POST["id"].".pdf'");
// PDF document source
readfile($conf['media']['pdf'] . $_POST["id"] . "/" . $_POST["id"].'.pdf');
