<?php defined('_WEXEC') or die;

/**
* vCard
*
* @on Form submit
* @method _POST
* @name vcard
*/

$contactFile = $conf['json']['contact'];

// If file exists
if(file_exists($contactFile)) {

  // Values
  $vCard[$_POST["vCard"]]['name'] = $_POST["name"];
  $vCard[$_POST["vCard"]]['address'] = $_POST["address"];
  $vCard[$_POST["vCard"]]['zip'] = $_POST["zip"];
  $vCard[$_POST["vCard"]]['city'] = $_POST["city"];
  $vCard[$_POST["vCard"]]['phone'] = $_POST["phone"];
  $vCard[$_POST["vCard"]]['mail'] = $_POST["mail"];
  $vCard[$_POST["vCard"]]['domain'] = $_POST["domain"];
  $vCard[$_POST["vCard"]]['stnr'] = $_POST["stnr"];
  $vCard[$_POST["vCard"]]['bank'] = $_POST["bank"];
  $vCard[$_POST["vCard"]]['iban'] = $_POST["iban"];
  $vCard[$_POST["vCard"]]['bic'] = $_POST["bic"];
  $vCard[$_POST["vCard"]]['nomwst'] = $_POST["nomwst"];

  // Put contents into JSON
  $newJsonString = json_encode($vCard);
  file_put_contents($contactFile, $newJsonString);

  // Redirect after Submit
  header('Location: index.php?page=contact');
} else {
  echo $contactFile . ' not found';
  die;
}
