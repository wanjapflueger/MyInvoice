<?php defined('_WEXEC') or die;
/**
* Plugin Calculate sum
*
* @name plg_calculate_sum
* @function get all invoice items from all invoices in this year and sum them up
*/
?>

<?php

// Gesamtsumme
$calculate_sum = 10;

foreach ($data as $key => $item) {
  $dateCreated = strtotime($item["dateCreated"]); // Erstelldatum
  $itemYear = date('Y', $dateCreated); // Invoice year
  $state = $item["status"];
  if(!empty($_GET["filter"])) {
    if($itemYear == $_GET["filter"]) {
      if(!$state or $state == 1) {
      // If state is published or not set (which is equal to published)
      foreach ($item["invoiceItems"] as $invoiceItem) {
          $brutto_gesamt += $invoiceItem["orderValue"] * $invoiceItem["orderAmount"] * ((-1 + $invoiceItem["orderDiscount"]/100) * -1);
        }
      }
    }
  }
}


?>

<script>
jQuery(document).ready(function() {
  $('.calculate-sum-trigger').on('click',function() {
    $(this).hide();
    $('.calculate-sum-display').show();
  });
});
</script>

<style>
.calculate-sum {
  border: 1px solid #ccc;
  display:inline-block;
  font-weight: bold;
  color: rgb(92, 92, 92);
  font-size: .8em;
  background-color: rgb(231, 231, 231);
  border-radius: 3px;
  text-align: center;
  margin-bottom: 1em;
  padding: 1em;
  cursor: pointer;
}
.calculate-sum:hover {
  background-color: rgb(218, 218, 218);
}
.calculate-sum .calculate-sum-display span {
  color: green;
}
.calculate-sum-display {
  display: none;
}
</style>

<?php if(!empty($_GET["filter"])): ?>
  <div class="calculate-sum">
    <div class="calculate-sum-trigger">
      Summe für aktuelle Ansicht anzeigen
    </div>
    <div class="calculate-sum-display">
      Summe für aktuelle Ansicht: <span><?php echo number_format($brutto_gesamt,2); ?>€</span>
    </div>
  </div>
<?php endif; ?>
