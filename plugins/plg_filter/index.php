<?php defined('_WEXEC') or die;
/**
* Filter results
*/
?>
<div class="sort-items">
  <ul>
    <?php
    // Filter from $startdate to now
    $startdate = $conf['startdate'];
    $enddate = date("Y");
    $years = range($startdate,$enddate);
    $years = array_reverse($years); // Show newest date first
    if($_GET["filter"]) {
      echo '<li><a class="removefilter" href="index.php">X</a></li>';
    }
    foreach ($years as $year) {
      if($_GET["filter"] == $year) {
        echo '<li><a class="current" href="?filter='.$year.'">'.$year.'</a></li>';
      } else {
        echo '<li><a class="" href="?filter='.$year.'">'.$year.'</a></li>';
      }
    }
    ?>
  </ul>
</div>
