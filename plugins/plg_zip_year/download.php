<?php
/**
* Create new Zip file
*/
$zipfile = 'archive.zip'; // filename
$zip = new ZipArchive();
$zip->open($zipfile, ZipArchive::CREATE);
// Add a .txt file with the current datetime
$zip->addFromString('readme.txt', date("c"));
// Get requested files from plg_zip_year/index.php
foreach ($_POST["file"] as $file) {
  $zip->addFile('../../'.$file.'',basename($file));
}
$zip->close();
// Set Header for Zip
header('Content-Type: application/zip');
header('Content-Length: ' . filesize($zipfile));
header('Content-Disposition: attachment; filename="'.$zipfile.'"');
readfile($zipfile);
unlink($zipfile); // unlink zip from server
?>
