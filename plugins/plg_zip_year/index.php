<?php defined('_WEXEC') or die;
/**
* Plugin Zip Year
*
* @name plg_zip_year
* @function get all invoice items from all invoices in this year, zip and download them
* @action POST["zipthisyear"]
*/
?>

<?php
if(isset($_POST["zipthisyear"])) {

  $i = 0; // error counter

  foreach ($data as $key => $item) {
    $dateCreated = strtotime($item["dateCreated"]); // Erstelldatum
    $itemYear = date('Y', $dateCreated); // Invoice year
    $state = $item["status"];
    if(!empty($_GET["filter"])) {
      if($itemYear == $_GET["filter"]) {
        if(!$state or $state == 1) {
          // If state is published or not set (which is equal to published)
          $file = $conf['media']['pdf'] . $item["id"] . "/" . $item["id"] . ".pdf";

          // Check if file exists
          if(file_exists($file)) {
            $msg .= "&check; " .$item["id"] . "<br />";

            // Add this $file to $files
            $files[] = $file;

          } else {
            $msg .= '<span class="error">&cross; Keine Datei für aktive Rechnung <a href="?id=' . $item["id"] . '">' . $item["id"] . '</a> gefunden.</span><br />';
            $i++;
          }

        }
      }
    }
  }

  // Check if any files were added to $files
  if(!empty($files)) {
    foreach ($files as $file) {
    }
  } else {
    $msg .= '<span class="error">Download '.$_GET["filter"].' PDF: Keine Dateien gefunden</span><br />';
  }

}

?>

<script>
jQuery(document).ready(function() {

});
</script>

<style>
.dlzip {
  display: inline-block;
}
.dlzip input {
  -webkit-appearance: none;
  border: 1px solid #ccc;
  display:inline-block;
  font-weight: bold;
  color: rgb(92, 92, 92);
  font-size: .8em;
  background-color: rgb(231, 231, 231);
  border-radius: 3px;
  text-align: center;
  margin-bottom: 1em;
  padding: 1em;
  cursor: pointer;
}
.dlzip input:hover {
  background-color: rgb(218, 218, 218);
}
.dlzip input:active {
  background-color: rgb(196, 196, 196);
}
.dlzip_confirm {
  border: 2px dashed rgb(21, 175, 36) !important;
}
</style>

<?php
if(!empty($_GET["filter"])):
if(isset($_POST["zipthisyear"])):
?>
<div class="msg">
  <?php if($files): ?>

  <?php if($i > 0): ?><span class="error"><?php echo $i; ?> Fehler</span><br /><br /><?php endif; ?>

  <form action="./plugins/plg_zip_year/download.php" style="display:block" method="post" class="dlzip">
    <?php
    if(isset($_POST["zipthisyear"])) {
      foreach ($files as $file) {
        echo '<input type="hidden" name="file[]" value="'.$file.'" />';
      }
    }
    ?>
    <!-- Submit and Download -->
    <input type="submit" value="Download .zip" class="dlzip_confirm" name="zipthisyeardownload" />
  </form>

  <?php
  endif; // if($files)
  ?>

  <!-- Display Message with error log -->
  <?php echo $msg; ?>
</div>

<?php
endif; // if(isset($_POST["zipthisyear"]))
?>

<?php if(!isset($_POST["zipthisyear"])): ?>

  <form action="" method="post" class="dlzip">
    <!-- Submit and Prepare -->
    <input type="submit" value="Create ZIP Archive of <?php echo $_GET["filter"]; ?>" name="zipthisyear" />
  </form>

<?php
endif; // if(!isset($_POST["zipthisyear"]))
endif; // if(!empty($_GET["filter"]))
