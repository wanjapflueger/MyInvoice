<?php defined('_WEXEC') or die;
/**
* All items list
*
* @class items
*/
?>

<?php include 'plugins/plg_filter/index.php'; // Filter ?>

<?php include 'plugins/plg_calculate_sum/index.php'; // Calculator ?>

<?php include 'plugins/plg_zip_year/index.php'; // ZIP ?>

<div class="items">
  <?php foreach ($data as $key => $item): ?>

    <?php
    /**
    * Values for each item
    *
    * @array $data[]
    * @key $key
    * @item $item
    * @class item
    */

    $id = $item["id"]; // Rechnungsnummer
    $dateCreated = strtotime($item["dateCreated"]); // Erstelldatum
    $company = $item["company"]; // Firma
    $firstName = $item["firstName"]; // Vorname
    $lastName = $item["lastName"]; // Nachname

    $itemYear = date('Y', $dateCreated); // Invoice year


    if(!empty($_GET["filter"])):
      if($itemYear == $_GET["filter"]):
      // show ivoices from $_GET["filter"]
    ?>

    <a
    class="item <?php echo ($item['status'] == -1) ? 'trashed' : '';?>"
    id=<?php echo $id; ?> href=index.php?id=<?php echo $id; ?>
    data-filter=<?php echo date("Y",$dateCreated); ?>
    >

      <!-- Datum -->
      <div class="dateCreated">
        <span class="day"><?php echo date('d', $dateCreated); ?></span>
        <span class="month"><?php echo date('M', $dateCreated); ?></span>
        <span class="year"><?php echo date('Y', $dateCreated); ?></span>
      </div>

      <div class="content">

        <!-- Rechnungsnummer -->
        <div class="renr">
          <?php echo $id; ?>
          <?php
          if(file_exists("media/pdf/" . $id . "/" . $id . '.pdf')) {
            $indicator = '<span style=color:green>✓</span>';
          } else {
            $indicator = '<span style=color:red>✘</span>';
          }
          echo '<span> | pdf '.$indicator.'</span>';
          ?>
        </div>

        <!-- Name und Firma der Rechnungsanschrift -->
        <div class="name">
          <span class="company"><?php echo $company; ?></span>
          <?php echo $firstName; ?>
          <?php echo $lastName; ?>
        </div>

      </div>

    </a>

  <?php
  endif;
  else:
  ?>
    <a
    class="item <?php echo ($item['status'] == -1) ? 'trashed' : '';?>"
    id=<?php echo $id; ?> href=index.php?id=<?php echo $id; ?>
    data-filter=<?php echo date("Y",$dateCreated); ?>
    >

      <!-- Datum -->
      <div class="dateCreated">
        <span class="day"><?php echo date('d', $dateCreated); ?></span>
        <span class="month"><?php echo date('M', $dateCreated); ?></span>
        <span class="year"><?php echo date('Y', $dateCreated); ?></span>
      </div>

      <div class="content">

        <!-- Rechnungsnummer -->
        <div class="renr">
          <?php echo $id; ?>
          <?php
          if(file_exists("media/pdf/" . $id . "/" . $id . '.pdf')) {
            $indicator = '<span style=color:green>✓</span>';
          } else {
            $indicator = '<span style=color:red>✘</span>';
          }
          echo '<span> | pdf '.$indicator.'</span>';
          ?>
        </div>

        <!-- Name und Firma der Rechnungsanschrift -->
        <div class="name">
          <span class="company"><?php echo $company; ?></span>
          <?php echo $firstName; ?>
          <?php echo $lastName; ?>
        </div>

      </div>

    </a>
  <?php
  // Show all
  endif;
  endforeach; ?>
</div>
