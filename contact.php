<?php defined('_WEXEC') or die;

/**
* Contact
*/

/**
* GET JSON
*
* @content contact 'My vCard'
*/
$contactFile = "data/contact.json"; // Filename
$jsonStringContact = file_get_contents($contactFile); // JSON String
$contact = json_decode($jsonStringContact, true); // contact array

foreach ($contact as $vCard):

?>
<div class="updateform">
  <h2>vCard</h2>





  <form action="" method="post">

    <div class="submit">
      <input type="submit" name="updatevcard" class="updatevcard" value="Save" />
      <a class="close" href="index.php">Close</a>
    </div>

    <?php
    /**
    * Form input fields
    */
    ?>
    <div class="field-list">

      <div class="field-group inline">
        <div class="field-item">
          <label for="name">Name</label>
          <input type="text" value="<?php echo $vCard["name"]; ?>" id="name" name="name" />
        </div>
      </div>

      <div class="field-group inline">
        <div class="field-item">
          <label for="address">Address</label>
          <input type="text" value="<?php echo $vCard["address"]; ?>" id="address" name="address" />
        </div>
        <div class="field-item">
          <label for="zip">ZIP</label>
          <input type="text" value="<?php echo $vCard["zip"]; ?>" id="zip" name="zip" />
        </div>
        <div class="field-item">
          <label for="city">City</label>
          <input type="text" value="<?php echo $vCard["city"]; ?>" id="city" name="city" />
        </div>
      </div>

      <div class="field-group inline">
        <div class="field-item">
          <label for="phone">Phone</label>
          <input type="text" value="<?php echo $vCard["phone"]; ?>" id="phone" name="phone" />
        </div>
        <div class="field-item">
          <label for="mail">Mail</label>
          <input type="text" value="<?php echo $vCard["mail"]; ?>" id="mail" name="mail" />
        </div>
        <div class="field-item">
          <label for="domain">Domain</label>
          <input type="text" value="<?php echo $vCard["domain"]; ?>" id="domain" name="domain" />
        </div>
      </div>

      <div class="field-group">
        <div class="field-item">
          <label for="stnr">StNr.</label>
          <input type="text" value="<?php echo $vCard["stnr"]; ?>" id="stnr" name="stnr" />
        </div>
      </div>

      <div class="field-group inline">
        <div class="field-item">
          <label for="bank">Bank</label>
          <input type="text" value="<?php echo $vCard["bank"]; ?>" id="bank" name="bank" />
        </div>
        <div class="field-item">
          <label for="iban">IBAN</label>
          <input type="text" value="<?php echo $vCard["iban"]; ?>" id="iban" name="iban" />
        </div>
        <div class="field-item">
          <label for="bic">BIC</label>
          <input type="text" value="<?php echo $vCard["bic"]; ?>" id="bic" name="bic" />
        </div>
      </div>

      <div class="field-group">
        <div class="field-item">
          <label for="nomwst">Klausel für Kleinunternehmer</label>
          <input type="text" value="<?php echo $vCard["nomwst"]; ?>" id="nomwst" name="nomwst" />
        </div>
      </div>

      <hr />

      <div class="field-group">
        <p>
          <i>You may update the following items via FTP only</i>
        </p>
        <div class="field-item">
          <label for="logo">Logo</label>
          <img src="<?php echo $conf['logo']; ?>" style="max-width:none;height:auto;width:100px;margin:.5em;">
          <input type="text" disabled value="<?php echo $conf['logo']; ?>" id="logo" name="logo" />
        </div>
        <div class="field-item">
          <label for="qr">QR</label>
          <img src="<?php echo $conf['qr']; ?>" style="min-width:none;max-width:none;height:auto;width:100px;margin:.5em;">
          <input type="text" disabled value="<?php echo $conf['qr']; ?>" id="qr" name="qr" />
        </div>
      </div>

    </div>

  </form>
</div>

<?php
endforeach;
